#include <cstddef>
#include <functional>
#include <iostream>
#include <iterator>
#include <optional>
#include <utility>

#define cat_(a, b) a##b
#define cat(a, b) cat_(a, b)

#define coro(T, body...)                                                  \
	coro_t<T> {                                                       \
		[=, __co_pos = (void *)0]() mutable -> std::optional<T> { \
			if (__co_pos) goto *__co_pos;                     \
			body;                                             \
			finish;                                           \
		}                                                         \
	}

#define yield(x)                                 \
	do {                                     \
		__co_pos = &&cat(co_, __LINE__); \
		return std::make_optional(x);    \
		cat(co_, __LINE__) : {}          \
	} while (0)

#define finish                                   \
	do {                                     \
		__co_pos = &&cat(co_, __LINE__); \
		cat(co_, __LINE__) : {}          \
		return std::nullopt;             \
	} while (0)

template <typename T>
class coro_t {
	using fn_t = std::function<std::optional<T>()>;

	class iter {
		std::optional<fn_t> m_co;
		std::optional<T> m_val;

	       public:
		using iterator_category = std::forward_iterator_tag;
		using value_type = T;
		using difference_type = ptrdiff_t;
		using pointer = T *;
		using reference = T &;

		explicit iter(std::optional<fn_t> co = std::nullopt)
		    : m_co{co} {
			++*this;
		}
		iter &operator++() {
			if (m_co) m_val = (*m_co)();
			return *this;
		}
		iter operator++(int) {
			iter ret = *this;
			++*this;
			return ret;
		}
		bool operator==(iter const &other) const {
			return !other.m_co && !m_val;
		}
		bool operator!=(iter const &other) const {
			return !(*this == other);
		}
		reference operator*() { return *m_val; }
	};

	fn_t m_co;

       public:
	explicit coro_t(fn_t co = []() { return std::nullopt; }) : m_co{co} {}
	std::optional<T> operator()() { return m_co(); }
	iter begin() { return iter{m_co}; }
	iter end() { return iter{}; }
};

auto fibs() {
	size_t a{1}, b{1};
	return coro(size_t, {
		for (;;) {
			yield(a);
			a = std::exchange(b, a + b);
		}
	});
}

template <typename C, typename T>
auto filter(C f, coro_t<T> c) -> coro_t<T> {
	std::optional<T> x;
	return coro(T, {
		while ((x = c())) {
			if (f(*x)) yield(*x);
		}
	});
}

template <typename T>
auto take(size_t n, coro_t<T> c) -> coro_t<T> {
	size_t i{};
	std::optional<T> x;
	return coro(T, {
		while ((x = c())) {
			if (i >= n) break;
			i++;
			yield(*x);
		}
	});
}

template <typename R, typename T, typename F>
auto lift2(F f, coro_t<T> a, coro_t<T> b) -> coro_t<R> {
	std::optional<T> x, y;
	return coro(R, {
		while ((x = a()) && (y = b())) {  //
			yield(f(*x, *y));
		}
	});
}

template <typename T>
auto reduce(std::function<T(T, T)> f, T a, coro_t<T> c) -> T {
	std::optional<T> x;
	while ((x = c())) a = f(*x, a);
	return a;
}

struct Node {
	int v;
	Node *l, *r;
};

auto vals(Node const &n) -> coro_t<int> {
	coro_t<int> c;
	std::optional<int> x;
	return coro(int, {
		if (n.l) {
			c = vals(*n.l);
			while ((x = c())) yield(*x);
		}
		yield(n.v);
		if (n.r) {
			c = vals(*n.r);
			while ((x = c())) yield(*x);
		}
	});
}

int main() {
	Node ll1{1};
	Node l1{2, &ll1};
	Node rr1{5};
	Node r1{4, {}, &rr1};
	Node n1{3, &l1, &r1};

	Node lr2{2};
	Node l2{1, {}, &lr2};
	Node rl2{4};
	Node r2{5, &rl2};
	Node n2{3, &l2, &r2};

	Node lr3{2};
	Node l3{1, {}, &lr3};
	Node rl3{3};
	Node r3{5, &rl3};
	Node n3{4, &l3, &r3};

	for (auto &n : std::vector{n1, n2, n3}) {
		auto c = vals(n);
		std::copy(std::begin(c), std::end(c),
			  std::ostream_iterator<int>(std::cout, " "));
		std::cout << std::endl;
	}

	auto ncmp = [](Node const &n1, Node const &n2) {
		return reduce<bool>(
		    std::logical_and{}, true,
		    lift2<bool, int>(std::equal_to{}, vals(n1), vals(n2)));
	};

	auto eq12 = ncmp(n1, n2);
	auto eq23 = ncmp(n2, n3);

	std::cout << "1st and 2nd " << (eq12 ? "" : "not ") << "equal"
		  << std::endl
		  << "2nd and 3rd " << (eq23 ? "" : "not ") << "equal"
		  << std::endl
		  << std::endl;

	auto c = take(20, filter([](auto x) { return x % 2 == 0; }, fibs()));

	std::copy(std::begin(c), std::end(c),
		  std::ostream_iterator<size_t>(std::cout, "\n"));
}
