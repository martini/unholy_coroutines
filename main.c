#include <stddef.h>
#include <stdio.h>

#include "coro.h"

coro(fibs, size_t *a; size_t b) {
	size_t *a = c->a, *b = &c->b;
	co_init;
	*a = 0, *b = 1;
	for (;;) {
		*b += *a, *a = *b - *a;
		co_yield;
	}
	co_finish;
}

coro(filter, bool (*test)(void *); void *arg; coro_t * inner) {
	co_init;
	while (co_await (c->inner)) {
		if (c->test(c->arg)) co_yield;
	}
	co_finish;
}

coro(take, size_t n; coro_t * inner) {
	co_init;
	while (c->n--) {
		if (!co_await (c->inner)) break;
		co_yield;
	}
	co_finish;
}

bool is_even(void *x) { return *(size_t *)x % 2 == 0; }

int main() {
	size_t i;
	take_coro_t ctx =
	    co_call(take, 20,
		    (coro_t *)&co_call(filter, is_even, &i,  //
				       (coro_t *)&co_call(fibs, &i)));
	while (co_await (&ctx)) printf("%zu\n", i);
}
