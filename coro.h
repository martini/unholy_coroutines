#pragma once

#include <stdbool.h>

typedef struct coro_t coro_t;
typedef bool (*coro_fn_t)(coro_t *);
struct coro_t {
	coro_fn_t body;
	void *state;
};

#define coro_labl_(l) co_##l
#define coro_labl(l) coro_labl_(l)

#define coro(name, args)       \
	typedef struct {       \
		coro_t __coro; \
		args;          \
	} name##_coro_t;       \
	bool name(name##_coro_t *c)

#define co_init                                                       \
	{                                                             \
		if (((coro_t *)c)->state) goto *((coro_t *)c)->state; \
	}

#define co_yield                                              \
	{                                                     \
		((coro_t *)c)->state = &&coro_labl(__LINE__); \
		return true;                                  \
		coro_labl(__LINE__) : {}                      \
	}

#define co_finish                                             \
	{                                                     \
		((coro_t *)c)->state = &&coro_labl(__LINE__); \
		coro_labl(__LINE__) : return false;           \
	}

#define co_await(ctx) (((coro_t *)(ctx))->body((coro_t *)(ctx)))
#define co_call(f, ...) ((f##_coro_t){{(coro_fn_t)f, NULL}, __VA_ARGS__})
